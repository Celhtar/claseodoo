odoo.define('mascotas.js', function (require) {
    'use strict';

    // Import odoo JS modules
    var ajax = require('web.ajax');
    var publicWidget = require('web.public.widget');
    var core = require('web.core');
    core.qweb.add_template('/mascotas/static/src/xml/mascotas.xml')

    // definicion de mi widget y registro en la lista de widgets publicos.
    publicWidget.registry.mascotasJs = publicWidget.Widget.extend({
        selector: '.mascotas',
        read_events: {
            'click .mascota': '_onClick',
        },
        mascota_data: undefined,
        start: async function() {
            var data = await ajax.jsonRpc('/mascota/hello2/');
            this.mascota_data = data;
            var template = core.qweb.render(
                'details',
                {
                    'mascotas': this.mascota_data
                });
            $('.mascotaresult').html(template);
        },
        _onClick: function(){
            alert('Hola');
        }
    });

})
