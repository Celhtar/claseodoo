from odoo import api, fields, models
from odoo.exceptions import (
    ValidationError,
    Warning as UserError
)
from datetime import date

class Mascota(models.Model):
    _name = "banariego.mascota" # Tabla banariego_mascota

    name = fields.Char('Nombre')
    age = fields.Integer('Edad')
    color = fields.Char('Color del pelage')
    owner_id = fields.Many2one('res.partner', string="Proprietario")
    consulta_ids = fields.One2many('banariego.mascota.consulta', 'mascota_id')
    num_consultas = fields.Integer('Numero de consultas', compute="_count_consultas")

    @api.depends('consulta_ids')
    @api.onchange('consulta_ids')
    def _count_consultas(self):
        for mascota in self:
            mascota.num_consultas = len(mascota.consulta_ids)

class Consulta(models.Model):
    _name = 'banariego.mascota.consulta' # Tabla banariego_mascota_consulta

    mascota_id = fields.Many2one('banariego.mascota', string="Mascota")
    fecha = fields.Date('Fecha consulta')
    motivo = fields.Char('Motivo')

    @api.onchange('fecha')
    def _check_date(self):
        for item in self:
            if item.fecha and item.fecha > date.today():
                raise UserError('No se puede tener consultas realizadas en el futuro.')


from odoo import api, models

class ParticularReport(models.AbstractModel):
    _name = 'report.mascotas.mascotas_consultas' # report.module_name.nombre_report

    @api.model
    def _get_report_values(self, docids, data=None):
        report_obj = self.env['ir.actions.report']
        report = report_obj._get_report_from_name('mascota.mascotas_consultas')
        docargs = {
            'doc_ids': docids,
            'doc_model': 'banariego.mascota',
            'docs': self,
            'data': [
                {
                    'name': 'Guenael',
                    'surname': 'Labois'
                },
                {
                    'name': 'Victor',
                    'surname': 'Aviles'
                },
                {
                    'name': 'Lajonner',
                    'surname': 'Crespin'
                }
            ]
        }
        return docargs