# -*- coding: utf-8 -*-
from odoo import http


class MascotaHttp(http.Controller):
    @http.route('/mascota/hello/', type='http', auth='public', website=True) 
    def index(self, **kw):
        mascotas = http.request.env['banariego.mascota'].search([])
        return http.request.render('mascotas.web_view', {'mascotas':mascotas})


    @http.route('/mascota/hello2/', type='json', auth='public', website=True) 
    def index2(self, **kw):
        mascotas = http.request.env['banariego.mascota'].search([])
        result=[]
        for m in mascotas:
            result.append({
                'name': m.name,
                'age': m.age,
                'color': m.color
            })
        return result