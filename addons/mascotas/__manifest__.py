# -*- coding: utf-8 -*-
{
    'name': "Mascotas",

    'summary': """
        Mascotas
        """,

    'description': """
        Mascotas
    """,

    'author': "OPA Consulting",
    'website': "http://www.opa-consulting.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Localization',
    'version': '1.2',

    # any module necessary for this one to work correctly
    'depends': [
        'base', 
        'contacts',
        'website',
        ],
    'demo': [
        
    ],
    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/menu.xml',
        'views/views.xml',
        'views/report.xml',
        'views/web.xml',
        ],
}
