import time

def memoize(f):
    memo = {}
    def helper(x):
        if x not in memo:            
            memo[x] = f(x)
        return memo[x]
    return helper


def factorial(n):
    time.sleep(1)
    return n * factorial(n-1) if n else 1

print(memoize(factorial)(4))
print(memoize(factorial)(5))