.PHONY: scaffold build push dev pre-commit help clean all
.DEFAULT_GOAL := help

# Variables del proyecto
PROJECT = claseodoo
FULLNAME = ClaseOdoo

# Variables para la distribucion
BRANCH = $(shell git rev-parse --abbrev-ref HEAD)
HASH = $(shell git rev-parse HEAD)
SHORT_HASH = $(shell git rev-parse --short HEAD)
DATE_PART = $(shell date +%d%m%Y)
USER = $(shell whoami)

##@ Ayuda
help:                ## Muestra esa ayuda.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)


##@ Inicializacion del proyecto
.git:                ## Inicializa un repositorio git.
	@git init

pre-commit: .git     ## Instala los hooks de pre-commit.
	@pre-commit install
	@pre-commit install --hook-type commit-msg

##@ Desarrollo
scaffold: run           ## Nuevo modulo. Usage: make MODULE=<module_name> scaffold
	@docker-compose exec web odoo scaffold $(MODULE) /mnt/extra-addons
	@sudo chown -R ${USER}:${USER} addons/$(MODULE)

chart: ## Genera la plantilla de plan de cuenta. make MODULE=<module_name> chart
	@python3 tools/account ${PROJECT} ${FULLNAME} accounts.csv groups.csv tax.csv params.txt $(MODULE)

run:  ## Levanta los servicios.
	@docker-compose up -d

stop: ## Para los servicios.
	@docker-compose down


##@ Distribucion
build: .git            ## Genera la imagen Docker.
	@docker build -t opaconsulting/${PROJECT}-${BRANCH}:latest -t opaconsulting/${PROJECT}-${BRANCH}:${DATE_PART}-${SHORT_HASH}  -f build.Dockerfile .

push: build        ## Sube la imagen Docker al repositorio de imagenes.
	@docker push opaconsulting/${PROJECT}-${BRANCH}:latest
	@docker push opaconsulting/${PROJECT}-${BRANCH}:${DATE_PART}-${SHORT_HASH}

dev: output                ## Genera el ambiente para pruebas y consultoria
	@cp ./odoo.conf ./${PROJECT}/odoo.conf
	@python3 tools/gen-stack ${PROJECT}-${BRANCH} ${DATE_PART} ${SHORT_HASH} > ./${PROJECT}/docker-compose.yml
	@zip -r "${PROJECT}-${BRANCH}-${DATE_PART}-${SHORT_HASH}" ./${PROJECT}
	@rm -rf ${PROJECT}
	
all: push dev ## Realiza todas las operaciones de distribucion: build push dev
	@echo Terminado !

output:
	@mkdir ${PROJECT}
	@mkdir ${PROJECT}/xml
	@mkdir ${PROJECT}/keys

##@ Limpieza
clean: ## Borra los artefactos construidos.
	@rm -rf *.zip