version: '2'
services:
  web:
    restart: always
    image: opaconsulting/${args.Project}:${args.Date}-${args.Hash}
    depends_on:
      - db    
    volumes:
      - odoo-web-data:/var/lib/odoo
      - ./keys:/opt/keys
      - ./xml:/opt/xml
    ports:
      - "8069:8069"
    environment:
      - DEBUG=0
  db:
    restart: always
    image: postgres:9.6
    environment:
      - POSTGRES_PASSWORD=odoo
      - POSTGRES_USER=odoo
      - POSTGRES_DB=postgres
      - PGDATA=/var/lib/postgresql/data/pgdata
    ports:
      - "5432:5432"
      
    volumes:
      - odoo-db-data:/var/lib/postgresql/data/pgdata

volumes:
  odoo-web-data:
  odoo-db-data: