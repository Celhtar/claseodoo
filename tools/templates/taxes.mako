<odoo>
    <data>

        % for tax in taxes:
        <record id="${tax.id}" model="account.tax.template">
            <field name="chart_template_id" ref="${args.project}_chart_template"/>
            <field name="name">${tax.name}</field>
            <field name="description">${tax.description}</field>
            <field name="amount">${tax.amount}</field>
            <field name="amount_type">${tax.type}</field>
            <field name="type_tax_use">${tax.use}</field>
            <field name="tax_group_id" ref="${tax.group}"/>
            <field name="invoice_repartition_line_ids" eval="[(5, 0, 0),
                (0,0, {
                    'factor_percent': 100,
                    'repartition_type': 'base',
                }),

                (0,0, {
                    'factor_percent': ${tax.base_percent},
                    'repartition_type': 'tax',
                    %if tax.account:
                    'account_id': '${tax.account}',
                    %endif
                }),
            ]"/>
            <field name="refund_repartition_line_ids" eval="[(5, 0, 0),
                (0,0, {
                    'factor_percent': 100,
                    'repartition_type': 'base',
                }),

                (0,0, {
                    'factor_percent': ${tax.base_percent},
                    'repartition_type': 'tax',
                    %if tax.refund_account:
                    'refund_account__id': '${tax.refund_refund}',
                    %endif
                }),
            ]"/>
        </record>
        %endfor

    </data>
</odoo>